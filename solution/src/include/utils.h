#ifndef UTILS_C
#define UTILS_C
#include "bmp.h"
#include <stdio.h>
#include <stdlib.h>

struct image read_image(FILE *image_file, int *error);
#endif
