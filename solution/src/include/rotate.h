#ifndef ROTATE_MODULE
#define ROTATE_MODULE
#include "bmp.h"
#include "image.h"

int rotate_image(struct image *image, FILE* output_file_name);
#endif
