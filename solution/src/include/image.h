#ifndef STD_IMAGE
#define STD_IMAGE
#include <stdint.h>
#include <stdio.h>

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct pixel { uint8_t b, g, r; };
void delete_image(struct image* img);
#endif
