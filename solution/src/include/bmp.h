#ifndef BMP_FILE
#define BMP_FILE
#include "image.h"
#include <stdint.h>
#include <stdio.h>

#pragma pack(push, 1)
struct bmp_header 
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};
#pragma pack(pop)

enum read_status {
    READ_OK = 0,
    READ_FAILED,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum write_status {
    WRITE_OK = 0,
    WRITE_INVALID_SOURCE,
    WRITE_HEADER_ERROR,
    WRITE_ERROR
};

enum read_status read_bmp_header(FILE* image_file, struct bmp_header *header);

enum read_status from_bmp( FILE* image_file, struct image *img );
enum write_status to_bmp( FILE* output_file, struct image const *img );
#endif
