#include "include/image.h"
#include <stdlib.h>

void delete_image(struct image* img) {
    if (img->data != NULL) {
        free(img->data);
    }
}
