#include "include/bmp.h"
#include "include/image.h"
#include <stdlib.h>

unsigned long compute_padding(uint64_t width) {
  unsigned long ans = (sizeof(struct pixel) * width) % 4;
  ans = 4 - ans;
  return ans == 4 ? 0 : ans;
}

enum read_status read_bmp_header(FILE *image_file, struct bmp_header *header) {
  unsigned long x = fread(header, sizeof(struct bmp_header), 1, image_file);
  if (x < 1) {
    fprintf(stderr, "Image header corrupted\n");
    return READ_INVALID_HEADER;
  }
  return READ_OK;
}

enum read_status from_bmp(FILE *image_file, struct image *img) {
  unsigned long padding = compute_padding(img->width);
  unsigned long x;
  for (size_t i = 0; i < img->height; i++) {
    x = fread(img->data + (i * img->width), sizeof(struct pixel), img->width,
              image_file);
    if (x < img->width) {
      fprintf(stderr, "Image data corrupted %zi\n", i);
      return READ_INVALID_BITS;
    }
    x = fseek(image_file, (long) padding, SEEK_CUR);
    if (x) {
      fprintf(stderr, "Image data corrupted, bad padding computation");
      return READ_INVALID_BITS;
    }
  }
  return READ_OK;
}

enum write_status write_header(FILE *output_file, struct image const *img) {
  struct bmp_header standard = {
      .bfType = 0x4d42,
      .bfReserved = 0,
      .bOffBits = 54,
      .biSize = 40,
      .biWidth = img->width,
      .biHeight = img->height,
      .biPlanes = 1,
      .biBitCount = 24,
      .biCompression = 0,
      .biSizeImage = img->height * (img->width * sizeof(struct pixel) +
                                    compute_padding(img->width)),
      .biXPelsPerMeter = 0,
      .biYPelsPerMeter = 0,
      .biClrUsed = 0,
      .biClrImportant = 0,
  };
  standard.bfileSize = standard.biSize + standard.biSizeImage;
  unsigned long x = fwrite(&standard, sizeof(struct bmp_header), 1, output_file);
  if (x < 1) {
    fprintf(stderr, "An error ocurred while writing header bmp output image\n");
    return WRITE_HEADER_ERROR;
  }
  return WRITE_OK;
}

enum write_status to_bmp(FILE *output_file, struct image const *img) {
    enum write_status result = write_header(output_file, img);
    if (result != WRITE_OK) {
        return result;
    }
    unsigned long x;
    unsigned long padding = compute_padding(img->width);
    for (size_t i = 0; i < img->height; i++) {
        x = fwrite(img->data + (i * img->width), sizeof(struct pixel), img->width,
                output_file);
        if (x < img->width) {
            fprintf(stderr, "An error ocurred while writing image data\n");
            return WRITE_INVALID_SOURCE;
        }
        x = fseek(output_file, (long) padding, SEEK_CUR);
        if (x) {
            fprintf(stderr, "Output Image could not be written because of padding computation");
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
