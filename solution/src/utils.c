#include "include/utils.h"

struct image read_image(FILE* image_file, int *error) {
    struct image ans = {0};
    struct bmp_header header;
    enum read_status read_error = read_bmp_header(image_file, &header);
    if (read_error != READ_OK) {
        *error = 1;
        return ans;
    }
    ans.width = header.biWidth;
    ans.height = header.biHeight;
    ans.data = (struct pixel*) malloc(sizeof(struct pixel) * ans.width * ans.height);
    if (ans.data == NULL) {
        *error = 1;
        fprintf(stderr, "Not enough memory available to complete operation, failed image loading\n");
        return ans;
    }
    long x = fseek(image_file, header.bOffBits, SEEK_SET);
    if (x) {
        delete_image(&ans);
        fprintf(stderr, "Input image file corrupted\n");
        return ans;
    }
    read_error = from_bmp(image_file, &ans);
    if (read_error != READ_OK) {
        delete_image(&ans);
        *error = 1;
        return ans;
    }
    return ans;
}
