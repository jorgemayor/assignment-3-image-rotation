#include "include/rotate.h"
#include <stdlib.h>

int rotate_image(struct image* image, FILE* output_file_name) {
    struct image rotated_image;
    rotated_image.height = image->width;
    rotated_image.width = image->height;
    rotated_image.data = (struct pixel*) malloc(sizeof(struct pixel) * image->height * image->width);
    if (rotated_image.data == NULL) {
        fprintf(stderr, "Not enough memory available to complete rotation\n");
        return 1;
    }
    for (size_t i = 0; i < image->height; i++) {
        for (size_t j = 0; j < image->width; j++) {
            rotated_image.data[j * image->height + (image->height - 1 - i)] = image->data[i * image->width + j];
        }
    }
    to_bmp(output_file_name, &rotated_image);
    free(rotated_image.data);
    return 0;
}
