#include "include/rotate.h"
#include "include/utils.h"
#include <stdio.h>

int main( int argc, char** argv ) {
    if(argc != 3) {
        fprintf(stderr, "Bad usage. Expected 2 arguments, but %i were found.\n", argc - 1);
        printf("Usage:\n./image-transformer <source-image> <name-transformed-image>\n");
        return 1;
    }

    FILE* image = NULL;
    image = fopen(argv[1], "rb");

    if(!image) {
        fprintf(stderr, "Image not found.\n");
        return 1;
    }

    int error = 0;
    struct image img = read_image(image, &error);
    if (error) {
        return 1;
    }

    fclose(image);

    FILE* rotated_image_file = NULL;
    rotated_image_file = fopen(argv[2], "wb");
    if (rotated_image_file == NULL) {
        delete_image(&img);
        fprintf(stderr, "Output file could not be created\n");
        fclose(image);
        return 1;
    }
    error = rotate_image(&img, rotated_image_file);
    delete_image(&img);
    fclose(rotated_image_file);
    if (error) {
        return error;
    }

    printf("Image rotated!\n");
    return 0;
}
